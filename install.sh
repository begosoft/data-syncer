#!/usr/bin/env bash

# Installation script for Data Syncer

WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CURDIR=$PWD

echo "The project repository folder is: '$WORKDIR'"
echo "The current working folder is: '$CURDIR'"
echo ""

NOW=`date "+%F %T"`
echo "Start installation at $NOW"

# Check if there is a python 3 and related packages installed

sudo apt-get install -y -q python3.4 python3-pip >/dev/null 2>/dev/null
pip3 install virtualenv

# Installing the project

virtualenv -p python3 .env
source ./.env/bin/activate

cd $WORKDIR
pip3 install .

cp samples/config.ini.sample $CURDIR/config.ini
sudo chmod 0766 $CURDIR/config.ini

NOW=`date "+%F %T"`
echo "Finish installation at $NOW"

cat << EOF
+--------------------------------------+"

The Data Syncer Project is installed.

Before running any of applications, please fill the configuration file.

To run the application of sync, execute in terminal:

$ source .env/bin/activate
$ dsync [--config-file=/path/to/config.ini]

To run the Web UI, execute in terminal:

$ source .env/bin/activate
$ dserver [--config-file=/path/to/config.ini]

If you don't specify an option 'config-file' or the environment variable 'DS_CONFIG', the config.ini file will be looked at the
current working directory.

Please enjoy yourselves! :)

EOF
