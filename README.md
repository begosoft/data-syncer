# The Data Syncer Project

## Description

This application synchronizes data between local file and external storage (local or non-local).

## Installation

**Warning**: This project was tested against Linux Ubuntu. All the deployment stuff is also related to the Ubuntu. If you want to use any other system - you'll need to update the deployment stuff and re-test the project.

To install the project, download it into a specific folder and run script:

```
$ sudo ./install.sh
```
## Configure the project

Before starting the application, you should update the configuration file.
By default, you will receive a sample file within the folder of your project, but the place could differ.

## Preparing to work with Google API

To enable working with Google Spreadsheets, you need to provide the required access to it via Google API. To do this, follow the next instructions:

- Go to the Google APIs Console.
- Create a new project.
- Click Enable API. Search for and enable the Google Drive API.
- Create credentials for a Web Server to access Application Data.
- Name the service account and grant it a Project Role of Editor.
- Download the JSON file - it's your client secret file. Make sure it won't come to any aliens!
- Copy the JSON file to your working directory and rename it to client_secret.json

## Run the application

Before starting the applications, make sure that you have set an environment variable "DS_CONFIG" to the path to your config.ini file.
Otherwise, this file will be looked at the current folder.

```
$ export DS_CONFIG=<path to your script>
```

To run the Syncer application, you should run in the terminal:

```bash
$ dsync
```

To run the WebUI application, you need to run it in the terminal:

```bash
$ dserver
```

## Additional information

If you need to see the list of application's functions, type

$ python main.py --help

## Authors

Copyright 2017-2019 BEGOSoft Inc. All rights reserved.
