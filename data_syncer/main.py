# Copyright 2017-2019 BEGOSoft Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from datetime import datetime
import sys
import time

from flask import Flask
from flask import render_template

from data_syncer.config import CONF
from data_syncer.logger import logger
from data_syncer.manager import Manager
from data_syncer.models import create_tables


def main():
    logger.info("\n\nStart Data Syncer at {}\n".format(datetime.now()))

    mode = CONF.get('main', 'mode')
    logger.info("Mode: {}".format(mode))

    frequency = int(CONF.get('main', 'frequency'))
    logger.info("Frequency: {} minutes".format(frequency))

    if mode in ('LOCAL', 'CACHE'):
        logger.info("Will use the database.")
        create_tables()

    manager = Manager(CONF.get('main', 'datafile'))
    frequency *= 60

    try:
        while True:
            logger.info("Updating data at {} ...".format(datetime.now()))
            manager.update()
            time.sleep(frequency)
    except KeyboardInterrupt:
        logger.warning("Got a Ctrl+C. Exiting...")

    return 0


app = Flask(__name__)


@app.route('/')
def index():

    manager = Manager(CONF.get('main', 'datafile'))
    data_src = manager.get()
    data = {'floats': ','.join([str(x[0]) for x in data_src]),
            'labels': ','.join(['"{}"'.format(x[1]) for x in data_src]),
            'integers': ','.join([str(x[2]) for x in data_src])}
    return render_template('index.html', data=data)


def run_server():
    try:
        app.run(host=CONF.get('auth', 'hostname'),
                port=8000,
                debug=False,
                use_reloader=False,
                processes=5)
    except KeyboardInterrupt:
        logger.warning("Got a Ctrl+C. Exiting...")

    return 0


if __name__ == '__main__':
    sys.exit(main())
