# Copyright 2017-2019 BEGOSoft Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import peewee

from data_syncer.config import CONF


db_filename = CONF.get('main', 'db_path')

_db = peewee.SqliteDatabase(db_filename)


# Here we create the basic model for the case if we'd like to add more models
class BaseModel(peewee.Model):
    class Meta(object):
        database = _db


class Data(BaseModel):
    float_value = peewee.FloatField()
    string_value = peewee.CharField()
    integer_value = peewee.IntegerField()
    boolean_value = peewee.BooleanField()
    last_updated = peewee.DateTimeField()


def create_tables():
    _db.create_tables([Data, ], safe=True)
