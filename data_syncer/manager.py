# Copyright 2017-2019 BEGOSoft Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import datetime

import gspread
from oauth2client.service_account import ServiceAccountCredentials

from data_syncer.config import CONF
from data_syncer.config import HEADERS
from data_syncer.config import SCOPES

from data_syncer.models import Data

from data_syncer.parser import Parser

from prettytable import PrettyTable


class GoogleWriter(object):

    def __init__(self, scopes, cred_file, sheet_name):
        self.scopes = scopes
        creds = ServiceAccountCredentials.from_json_keyfile_name(cred_file, scopes)
        client = gspread.authorize(creds)

        self.sheet = client.open(sheet_name).sheet1

    def write(self, data):
        self.sheet.clear()

        for i in range(len(data)):
            for j in range(len(data[i])):
                self.sheet.update_cell(i + 1, j + 1, data[i][j])


class Manager(object):

    def __init__(self, test_filename):

        self._fname = test_filename
        self.last_updated = None
        self.parser = Parser()
        self.mode = CONF.get('main', 'mode')

    def get(self):
        if self.mode not in ('CACHE', 'LOCAL'):
            return self.parser.parse(self._fname)

        data = []
        for obj in Data.select():
            data.append([obj.float_value,
                         obj.string_value,
                         obj.integer_value,
                         obj.boolean_value])
        return data

    def update(self):
        data = self.parser.parse(self._fname)
        self.last_updated = datetime.datetime.now()

        if self.mode == 'INTERNET':
            writer = GoogleWriter(SCOPES,
                                  CONF.get('auth', 'creds_file_path'),
                                  CONF.get('main', 'sheet_name'))
            writer.write(data)
            return

        elif self.mode == 'LIVE':
            pt = PrettyTable(HEADERS)
            pt.align[HEADERS[0]] = "l"
            for obj in data:
                pt.add_row(obj)

            print(pt)

            return

        elif self.mode == 'PURGE':
            raise NotImplementedError("PURGE MODE")

        elif self.mode == 'CACHE':
            Data.delete().execute()

        # for LOCAL and CACHE
        for obj in data:
            Data.create(float_value=obj[0],
                        string_value=obj[1],
                        integer_value=obj[2],
                        boolean_value=obj[3],
                        last_updated=self.last_updated)
