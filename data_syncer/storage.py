# Copyright 2017-2019 BEGOSoft Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta
from abc import abstractmethod


class DataStorage(metaclass=ABCMeta):
    """The Storage class decorator for different storage backends.
    This is the base class, while custom derived classes are used
    for different modes.
    """

    @abstractmethod
    def update(self, new_data):
        """Update all the data in the storage."""
        pass

    @abstractmethod
    def data(self):
        """Returns data stored in the storage."""
        pass

    @abstractmethod
    def clear(self):
        """Clean out all the data in the storage."""
        pass


class LiveStorage(object):

    def __init__(self):
        self._data = []

    def update_data(self, new_data):
        if not isinstance(new_data, tuple) and not isinstance(new_data, list):
            raise TypeError("Unsupported data type: {}".format(type(new_data)))

        self._data = new_data

    @property
    def data(self):
        return self._data


MODES = [LiveStorage, ]

for cls in MODES:
    DataStorage.register(cls)
