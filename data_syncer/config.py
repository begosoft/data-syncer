# Copyright 2017-2019 BEGOSoft Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import configparser
import os


MODES = ['LIVE', 'LOCAL', 'CACHE']
HEADERS = ["Float", "String", "Integer", "Boolean"]

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

DEFAULT_CONFIG_PATH = os.path.join(os.getcwd(), 'config.ini')


def init_config():
    parser = configparser.ConfigParser()
    path = os.environ['DS_CONFIG'] if os.environ.get('DS_CONFIG') else DEFAULT_CONFIG_PATH
    print("PATH", path)
    parser.read(path)

    return parser


CONF = init_config()
