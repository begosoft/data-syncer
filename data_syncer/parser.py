# Copyright 2017-2019 BEGOSoft Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import csv
import xlrd


class Parser(object):
    """This class provides functionality for parsing CSV and XLS files
    transparently.
    """

    def __init__(self):
        pass

    def _load_csv(self, fname):
        result = []
        with open(fname, 'r') as fp:
            rd = csv.reader(fp, delimiter=',')
            for row in rd:
                result.append(row)

            for i in range(len(result)):
                result[i][0] = float(result[i][0])
                result[i][2] = int(result[i][2])
                result[i][3] = (result[i][3] == 'True')

        return result

    def _load_xlsx(self, fname):
        wb = xlrd.open_workbook(fname)
        sheet = wb.sheet_by_name('Data')
        result = []

        for i in range(sheet.nrows):
            result.append([])
            for j in range(sheet.ncols):
                # In case we have a string for boolean value
                value = sheet.cell(i, j).value
                if j == 2:
                    # xlrd actually cannot read integer properly
                    result[i].append(int(value))
                elif j != 3 or isinstance(value, bool):
                    result[i].append(value)
                elif isinstance(value, str):
                    result[i].append(value == 'True')
                elif isinstance(value, int):
                    result[i].append(bool(value))

        return result

    def parse(self, fname):
        """Opens the file by 'fname' and returns its data
        already prepared as a Python-compatible datatype.
        """

        result = []

        if fname.endswith('csv'):
            result = self._load_csv(fname)
        elif fname.endswith('xlsx'):
            result = self._load_xlsx(fname)
        else:
            raise TypeError("Unsupported file type for parsing")

        return result
