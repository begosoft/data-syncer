# Copyright 2017-2019 BEGOSoft Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import datetime
import os
import random
import unittest
import xlwt

from data_syncer.parser import Parser


# This is important, as we need to have a config file
# specified before the ConfigParser initialization.
os.environ['DS_CONFIG'] = os.path.join(os.path.dirname(__file__), 'test_config.ini')


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        # generate test file
        self.parser = Parser()

        self.TEST_LIMIT = 100
        self.ROW_LENGTH = 4
        self.ukey = str(datetime.datetime.now()).replace(
            ':', '').replace('.', '').replace(' ', '').replace('-', '')

        self.fps = {'csv': None, 'xlsx': None}
        self._fill_test_files()

    def tearDown(self):
        for fkey in self.fps:
            if self.fps[fkey]:
                self.fps[fkey].close()

    def test_file_name(self, ext='xlsx'):
        return os.path.join('/tmp', 'test_{}.{}'.format(self.ukey, ext))

    def _prepare_test_data(self):

        data = []
        for i in range(self.TEST_LIMIT):
            data.append([random.random() * 100,
                         'string_' + str(i + 1),
                         random.randint(1, 1024),
                         random.random() < 0.5])

        return data

    def _fill_csv(self, new_data):
        if self.fps['csv']:
            self.fps['csv'].close()
        self.fps['csv'] = open(self.test_file_name('csv'), 'w')
        for line in new_data:
            self.fps['csv'].write(','.join(map(str, line)) + '\n')
        self.fps['csv'].close()

    def _fill_xlsx(self, new_data):
        wb = xlwt.Workbook()
        ws = wb.add_sheet('Data')

        for i in range(len(new_data)):
            for j in range(len(new_data[i])):
                ws.write(i, j, new_data[i][j])
        wb.save(self.test_file_name())

    def _fill_test_files(self):
        _data = self._prepare_test_data()

        for key in self.fps:
            try:
                getattr(self, '_fill_' + key)(_data)
            except AttributeError:
                pass  # logging

    @property
    def data(self):
        self.fp.seek(0)
        return self.fp.read().decode()
