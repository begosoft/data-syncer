# Copyright 2017-2019 BEGOSoft Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from .base import BaseTestCase


class ParserTestCase(BaseTestCase):

    def setUp(self):
        super(ParserTestCase, self).setUp()

    def test_getting_data(self):
        # It's ensured in BaseTestCase that the data file is correct
        data = [self.parser.parse(self.test_file_name(key)
                                  ) for key in self.fps]

        for content in data:
            self.assertTrue(isinstance(content, list))
            self.assertEqual(len(content), self.TEST_LIMIT)

            for line in content:
                self.assertEqual(len(line), self.ROW_LENGTH)
                self.assertTrue(isinstance(line[0], float))
                self.assertTrue(isinstance(line[1], str))
                self.assertTrue(isinstance(line[2], int))
                self.assertTrue(isinstance(line[3], bool))
